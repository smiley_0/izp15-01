/**
 * @file main.c
 *
 * IZP Project 01
 *
 * @author    Adrian Kiraly (xkiral01@stud.fit.vutbr.cz) [1BIA]
 * @date      2015-10-03
 * @version   1.1
 *
 * Program reads input from stdin, detects various kinds of text input (such as
 * date or number) and further processes it according to set rules.
 * More information on usage can be obtained by running the compiled binary with
 * any arguments.
 */

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <limits.h>
#include <stdbool.h>
#include <time.h>
#include <ctype.h>

typedef long long int LLint;

// Constants
// =============================================================================

/// Error code set by scan_string if there's a null byte on input
#define ERR_UNEXP_INPUT 42

/// Error code returned by create_date if no valid date could be created
#define ERR_NO_VAL_DATE -47

/// Maximum number of characters to read-in at once
#define MAX_CHAR_READ 100

/// Default mask for date comparison
const char DATE_FORMAT[] = "DDDD-DD-DD";


// Enums
// =============================================================================

/**
 * @brief    Output types
 *
 * Used for printing information about type of detected input
 */
typedef enum {
    /// Valid date
    OTYPE_DATE,

    /// Number
    OTYPE_NUMBER,

    /// Word
    OTYPE_WORD,

    /// Error
    OTYPE_ERROR
} TOutputType;


/**
 * @brief    Additional output parameters.
 *
 * Used for printing additional information about detected input (palindromes,
 * primes)
 */
typedef enum {
    /// No output parameter
    PAR_NULL,

    /// Prime number parameter
    PAR_PRIME,

    /// Palindrome parameter
    PAR_PALINDROME
} TOutputParameters;


// Localization strings
// =============================================================================

/// Localized strings for output types
const char *LOC_OUTPUTTYPES[] = {
    [OTYPE_DATE] = "date",
    [OTYPE_NUMBER] = "number",
    [OTYPE_WORD] = "word",
    [OTYPE_ERROR] = "error"
};

/// Localized strings for output parameters
const char *LOC_OUTPUTPARAMETERS[] = {
    [PAR_NULL] = "",
    [PAR_PRIME] = "prime",
    [PAR_PALINDROME] = "palindrome"
};

/// Localized strings for days of week
const char *LOC_DAYSOFWEEK[] = {
    "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"
};

/// Localized string for null byte error
const char LOC_UNEXP_INPUT[] = "Unexpected input. Terminating.";


// Utility functions
// =============================================================================

/**
 * @brief      Gets string length
 *
 * Counts the number of characters from 0-th position to first found `\0`
 *
 * @param[in]  string  Input string
 *
 * @return     Length of the string
 */
int string_len(const char *string) {
    int i = 0;

    while (string[i] != '\0') {
        i++;
    }

    return i;
}


/**
 * @brief      Checks whether character is not a number (`[0-9]`)
 *
 * @param[in]  c     Input character
 *
 * @return     `true` or `false`, depending on the result of comparison
 */
bool is_not_digit(char c) {
    return (c < '0' || c > '9');
}


// Processing functions
// =============================================================================

/**
 * @brief      Reads string from stdin up to first whitespace
 *
 * Behaves similarly as `scanf("%s")`, but:
 *  - Reads only `maxlen` characters at most, thus provides overflow protection.
 *    Remaining characters will be read on the next call of function.
 *  - If there's a null byte found on input, immediately stops reading and sets
 *    `errflag` to `ERR_UNEXP_INPUT`. Any characters read up to the null byte
 *    will still be returned and available for processing (this also means it
 *    may return an empty string in this case)
 *  - End of file is returned via `errflag` as well
 *  - It will discard any leading whitespace
 *
 * @param[in]  maxlen   Maximum length of string
 * @param      string   Pointer to char array where the resulting string will be
 *                      saved
 * @param      errflag  Pointer to an integer variable for error codes
 *
 * @return     Number of successfully read characters
 */
int scan_string(int maxlen, char *string, int *errflag) {
    int i = 0;

    while (i < maxlen) {
        int c = getchar();

        if (c == '\0') {
            // Unexpected character
            *errflag = ERR_UNEXP_INPUT;
            break;
        }

        if (c == EOF) {
            // Return read chars and EOF
            *errflag = EOF;
            break;
        }

        if (isspace(c)) {
            if (i > 0) {
                // Return read chars
                *errflag = 0;
                break;
            }
            else {
                // Discard leading whitespace
                continue;
            }
        }

        // Else append the character to string
        string[i] = c;
        i++;
    }

    // Add string terminator and return
    string[i] = '\0';
    return i;
}


/**
 * @brief      Compares given string with a specified format
 *
 * Compares whether each character of input string matches specified format.
 * Supported format specifiers:
 *  - `D` matches a single `[0-9]` digit
 *  - `-` matches a dash
 *
 * @param      string  Input string
 * @param      format  Format string
 *
 * @return     Result of comparison.
 *             Returns `false` if string doesn't match specified format or
 *             format includes invalid characters. Otherwise returns `true`
 */
bool matches_format(const char *string, const char *format) {
    int i = 0;

    while (string[i] != '\0' || format[i] != '\0') {
        switch (format[i]) {

            // Matches a single [0-9] digit
            case 'D':
                if (is_not_digit(string[i])) {
                    return false;
                }
                break;

            // Matches a single dash sign
            case '-':
                if (string[i] != '-') {
                    return false;
                }
                break;

            // Invalid format specifier
            //
            // This also catches \0 in format string when compared input is
            // longer than format string and correctly returns that the string
            // does not match
            default:
                return false;
                break;
        }

        i++;
    }

    return true;
}


/**
 * @brief      Creates a string containing abbreviated weekday name and date
 *
 * @param[in]  date_string    String containing date in format `YYYY-MM-DD`
 * @param      target_string  Pointer to char array where the resulting string
 *                            will be copied
 * @param[in]  ts_length      Size of the destination array
 *
 * @return     Return value of `snprintf()` or `ERR_NO_VAL_DATE` if date
 *             couldn't been converted
 */
int create_date(const char *date_string, char *target_string, size_t ts_length) {
    // Extract values from string
    int year, month, day;
    sscanf(date_string, "%4d-%2d-%2d", &year, &month, &day);

    // Verify ranges
    if (!(day >= 1 && day <= 31)) {
        return ERR_NO_VAL_DATE;
    }

    if (!(month >= 1 && month <= 12)) {
        return ERR_NO_VAL_DATE;
    }

    struct tm date = {
        .tm_year = year - 1900,
        .tm_mon  = month - 1,
        .tm_mday = day,
    };

    // Try calling mktime
    if (mktime(&date) == -1) {
        return ERR_NO_VAL_DATE;
    }

    int wday = date.tm_wday;

    return snprintf(target_string, ts_length, "%s %s", LOC_DAYSOFWEEK[wday], date_string);
}


/**
 * @brief      Detects if string is a number
 *
 * Works on arbitrarily long string as the comparison is done on per-character
 * basis
 *
 * @param[in]  string  Input string
 *
 * @return     `true` if string contains only digits, otherwise `false`
 */
bool is_number(const char *string) {
    int i = 0;

    while (string[i] != '\0') {
        if (is_not_digit(string[i])) {
            return false;
        }

        i++;
    }

    return true;
}


/**
 * @brief      Tests primality of number
 *
 * Tests primality of given number using a simple trial division. Only numbers
 * in range (1;INT_MAX] get tested
 *
 * @param[in]  number  Input number
 *
 * @return     `PAR_PRIME` if number is prime and in range, otherwise `PAR_NULL`
 */
TOutputParameters is_prime(LLint number) {
    // Only numbers greater than 1 and smaller than INT_MAX should get tested
    if (number <= 1 || number > INT_MAX) {
        return PAR_NULL;
    }

    // Speculatively cast number to 32 bit for a significant speedup
    int i32number = number;

    for (int i = 2; i <= i32number / 2; ++i) {
        if (i32number % i == 0) {
            return PAR_NULL;
        }
    }

    return PAR_PRIME;
}


/**
 * @brief      Tests whether a given string is a palindrome
 *
 * @param[in]  string  Input string
 *
 * @return     `PAR_PALINDROME` if string is a palindrome, otherwise `PAR_NULL`
 */
TOutputParameters is_palindrome(const char *string) {
    int string_length = string_len(string);

    for (int i = 0; i < (string_length / 2) + 1; ++i) {
        if (string[i] != string[string_length-1-i]) {
            return PAR_NULL;
        }
    }

    return PAR_PALINDROME;
}


// Output functions
// =============================================================================

/**
 * @brief      Prints detection results
 *
 * Prints formated string with specifield results
 *
 * @param[in]  type    Detected string type
 * @param[in]  string  String representation of result
 * @param[in]  par     Additional output parameter
 */
void print_result(TOutputType type, const char *string, TOutputParameters par) {
    printf("%s: %s", LOC_OUTPUTTYPES[type], string);

    if (par != PAR_NULL) {
        printf(" (%s)", LOC_OUTPUTPARAMETERS[par]);
    }

    printf("\n");
}


/**
 * @brief      Prints help message
 */
void print_help(const char *binary_name) {
    printf(
    "usage: %s [anything]\n\n"
    "When launched with no arguments, reads whitespace delimited words until\n"
    "the end of file and does one of following:\n"
    "  - for date in 'YYYY-MM-DD' format calculates day of week\n"
    "  - for positive integer with no signs tests primality\n"
    "  - anything else is considered as a word and gets tested whether it is\n"
    "    a palindrome\n\n"
    "Running with any arguments displays this message.\n\n"
    "-----------------------------------------------------------------------\n"
    "IZP Project 01 / Working with text\n\n"
    "version 1.1                                        Adrian Kiraly [1BIA]\n"
    "compiled on: %s %s            xkiral01@stud.fit.vutbr.cz\n",
    binary_name, __DATE__, __TIME__);
}


// Main function
// =============================================================================

/**
 * @brief      Main function
 *
 * @param[in]  argc  Argument count
 * @param[in]  argv  Argument vector (unused)
 *
 * @return     Return code
 */
int main(int argc, char const *argv[]) {
    // Silence the unused parameter warning
    (void) argv;

    // Check for any arguments
    if (argc >= 2) {
        // Print help message and exit
        print_help(argv[0]);
        return EXIT_SUCCESS;
    }

    char input_buffer[MAX_CHAR_READ + 20];  // Just in case

    while (true) {
        int errflag = 0;
        int readcnt = scan_string(MAX_CHAR_READ, input_buffer, &errflag);

        // Run detection only if there is at least one char read
        if (readcnt > 0) {
            // Detect input type
            if (matches_format(input_buffer, DATE_FORMAT)) {
                // Size of string for result
                // There should be absolutely no need to change this, unless
                // you're trying to change the format of output
                const size_t result_size = 20;

                char result[result_size];

                // Try creating the date
                if (create_date(input_buffer, result, result_size) > 0) {
                    // Successful if at least 1 char returned
                    print_result(OTYPE_DATE, result, PAR_NULL);
                }
                else {
                    // If date creation was unsuccessful, then the input is
                    // certainly a word. It's also definitively not a palindrome
                    // as it matches the format string for date
                    print_result(OTYPE_WORD, input_buffer, PAR_NULL);
                }
            }
            else if (is_number(input_buffer)) {
                errno = 0;
                LLint number = strtoll(input_buffer, NULL, 10);
                TOutputParameters parameter = PAR_NULL;

                if (errno != ERANGE) {
                    parameter = is_prime(number);
                    snprintf(input_buffer, MAX_CHAR_READ, "%lld", number);
                }

                print_result(OTYPE_NUMBER, input_buffer, parameter);
            }
            else {
                TOutputParameters parameter = is_palindrome(input_buffer);

                print_result(OTYPE_WORD, input_buffer, parameter);
            }
        }

        // Handle unexpected input by terminating
        if (errflag == ERR_UNEXP_INPUT) {
            print_result(OTYPE_ERROR, LOC_UNEXP_INPUT, PAR_NULL);
            return ERR_UNEXP_INPUT;
        }

        // Properly exit on end of file
        if (errflag == EOF) {
            break;
        }
    }

    return EXIT_SUCCESS;
}
