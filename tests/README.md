Testcases
=========

What?
-----

This file contains human-readable descriptions of included test cases. Ideal for in-detail information about various caveats in testcases. Leave this text here and add your stuff below.

Naming conventions
------------------

### Suffixes
Each testcase should have an input file (suffixed with `.in`) and sample output file (suffixed with `.ou`)

### Prefixes
- `O.` for testcases in assignment
- `H.` for manually created testcases
- `R.` for testcases generated randomly
- `C.` for testcases with caveats

Test Cases
------------

### C.01
This TC may cause invalid detection when using sscanf and wrongly identify numerical strings ending with - as numbers instead of words

### C.02
Dates with invalid number of digits should get matched as word

### C.03
Arbitrarily long positive numbers should always get matched, even if they are significantly larger than INT_MAX

### C.04
Null byte should cause immediate termination with an error message. Only characters with values 1-255 should be considered valid [(src)](https://wis.fit.vutbr.cz/FIT/st/phorum-msg-show.php.cs?id=41173)

### C.05
Palindrome detection should be case sensitive [(src)](https://wis.fit.vutbr.cz/FIT/st/phorum-msg-show.php.cs?id=40945)

### C.06
Zero and one are NOT primes

### H.01
Test of each valid character. Words should get matched as palindromes.

### H.02
First 10,000 prime numbers. This is more of a performance test

### H.03
Only numbers up to INT_MAX should get tested for primes

### H.04
Basic testing of number detection

### H.05
Various dates, some out of tm struct range
